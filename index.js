const galleryContainer = document.querySelector(".gallery-container");
const currPhotos = [];

const getRandomSize = (min, max) => {
  return Math.round(Math.random() * (max - min) + min);
};

const renderGalleryItem = async (i) => {
  const currDiv = document.querySelector(`.box-${i}`);
  const response = await fetch(`https://source.unsplash.com/random?sig=${i}/`);

  currDiv.innerHTML = `
        <img class="gallery-image" src="${response.url}" alt="gallery image"/>
      `;
};

for (let i = 1; i < 101; i++) {
  renderGalleryItem(i);
}
